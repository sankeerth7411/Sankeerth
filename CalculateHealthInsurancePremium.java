package in.co.test.emids;

import java.util.Scanner;

public class CalculateHealthInsurancePremium {
	
	public static void main(String[] args) {
		try{
		Scanner input = new Scanner(System.in);
		System.out.println("Enter details below.");
		
		System.out.println("Name ");
		String name = input.next();
		
		System.out.println("Age ");
		int age = input.nextInt();
		
		
		System.out.println("Gender ");
		String gender = input.next();
		 
		 System.out.println("Hypertension ");
		 String hypertension = input.next();
		 
		 System.out.println("Blood Pressure");
		 String bloodPressure = input.next();
		 
		 System.out.println("Bood Sugar ");
		 String boodSugar = input.next();
		 
		 System.out.println("Over Weight ");
		 String overWeight = input.next();
		 
		 System.out.println("Smoking");
		 String smoking = input.next();
		 
		 System.out.println("Alcohol ");
		 String alcohol = input.next();
		 
		 System.out.println("Daily Exercise ");
		 String dailyExercise = input.next();
		 
		 System.out.println("Drugs ");
		 String drugs = input.next();
		
		 PatientDetails patientDetails = new PatientDetails(name, age, gender, hypertension, bloodPressure, boodSugar, overWeight, smoking, alcohol, dailyExercise, drugs);
		
		 int premium = calculatePercentage(patientDetails);
		 System.out.println("Health Insurance Premium for Mr."+ patientDetails.getName()+": Rs."+premium);
		 
		 
		
		}catch(Exception e){
			 e.printStackTrace();
		 }
	}
	//Calculate patient health insurance Premium 
	public static int calculatePercentage(PatientDetails patientDetails){
		int basePrice = 5000;

		if(patientDetails!=null){
		int age = patientDetails.getAge();
		String gender = patientDetails.getGender(); 
		String hypertension = patientDetails.getHypertension();
		String bloodPressure = patientDetails.getBloodPressure();
		String bloodSugar = patientDetails.getBoodSugar();
		String overweight = patientDetails.getOverWeight();
		String dailyExercise = patientDetails.getDailyExercise();
		String smoking = patientDetails.getSmoking();
		String alcohol = patientDetails.getAlcohol();
		String drugs = patientDetails.getDrugs();
		
		 int tempBasePrice=basePrice;
		 
		 if(age >=18 || age <=40){
			 tempBasePrice =( (10*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
		 }else if(age>40){
			 tempBasePrice =( (20*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
		 }
		 
		 tempBasePrice=basePrice;
		 if(gender!=null  &&  !gender.equals("") && gender.equalsIgnoreCase("Male")){
			 tempBasePrice =( (20*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
		 }
		 tempBasePrice=basePrice;
		 if(hypertension.equalsIgnoreCase("Yes") || bloodPressure.equalsIgnoreCase("Yes") || bloodSugar.equalsIgnoreCase("Yes") || overweight.equalsIgnoreCase("Yes")){
			 tempBasePrice =( (1*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
			 
		 }
		 tempBasePrice=basePrice;
		 if(dailyExercise.equalsIgnoreCase("Yes")){
			 tempBasePrice =( (-3*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
		 }
		 
		 tempBasePrice=basePrice;
		 if(smoking.equalsIgnoreCase("Yes") || alcohol.equalsIgnoreCase("Yes") || drugs.equalsIgnoreCase("Yes")){
			 tempBasePrice =( (3*tempBasePrice)/100);
			 basePrice+=tempBasePrice;
		 }
		}
		 return basePrice;
		 
	}

}
